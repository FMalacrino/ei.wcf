﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using EI.WCF.Service;

namespace EI.WCF.Server
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Step 1: Create a URI to serve as the base address.
            Uri baseAddress = new Uri("http://localhost:8000/EI.WCF.Service/CalculatorService");

            // Step 2: Create a ServiceHost instance.
            ServiceHost host = new ServiceHost(typeof(OperationService));

            try
            {
                // Step 3: Add a service endpoint.
                host.AddServiceEndpoint(typeof(IOperation), new BasicHttpBinding(), baseAddress);

                //// Step 4: Enable metadata exchange.
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                smb.HttpGetUrl = baseAddress;
                host.Description.Behaviors.Add(smb);
                // http://localhost:8000/EI.WCF.Service/CalculatorService pour voir le wsdl
                // Utiliser la commande suivante pour générer le client:
                // "C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.8 Tools\SvcUtil.exe" /language:cs /out:generatedProxy.cs /config:app.config http://localhost:8000/EI.WCF.Service/CalculatorService

                // Step 5: Start the service.
                host.Open();
                Console.WriteLine("The service is ready.");

                // Close the ServiceHost to stop the service.
                Console.WriteLine("Press <Enter> to terminate the service.");
                Console.WriteLine();
                Console.ReadLine();
                host.Close();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("An exception occurred: {0}", ce.Message);
                host.Abort();
            }
        }
    }
}