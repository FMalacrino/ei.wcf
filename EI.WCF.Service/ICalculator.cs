﻿using System.ServiceModel;

namespace EI.WCF.Service
{
    [ServiceContract(Namespace = "http://EI.WCF.Service")]
    public interface ICalculator
    {
        [OperationContract]
        double Add(double n1, double n2);

        [OperationContract]
        double Subtract(double n1, double n2);

        [OperationContract]
        double Multiply(double n1, double n2);

        [OperationContract]
        double Divide(double n1, double n2);
    }
}