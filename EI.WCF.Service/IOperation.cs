﻿using System.Runtime.Serialization;
using System.ServiceModel;

namespace EI.WCF.Service
{
    [ServiceContract(Namespace = "http://EI.WCF.Service")]
    public interface IOperation
    {
        [OperationContract]
        OperationMessage Calculate(OperationMessage operation);
    }

    
}