﻿namespace EI.WCF.Service
{
    public class OperationService : IOperation
    {
        private readonly CalculatorService calculatorService;

        public OperationService()
        {
            calculatorService = new CalculatorService();
        }

        public OperationMessage Calculate(OperationMessage message)
        {
            OperationMessage response = new OperationMessage();

            if (message.Token != "token")
            {
                response.Info = "Votre application n'est pas autorisée à communiquer avec nos serveurs.";
                return response;
            }

            double a = message.Operand1;
            double b = message.Operand2;

            switch (message.Name)
            {
                case "Add":
                    response.Result = calculatorService.Add(a, b);
                    break;

                case "Subtract":
                    response.Result = calculatorService.Subtract(a, b);
                    break;

                case "Multiply":
                    response.Result = calculatorService.Multiply(a, b);
                    break;

                case "Divide":
                    response.Result = calculatorService.Divide(a, b);
                    break;

                default:
                    response.Info = "Cette opération n'est pas valide";
                    return response;
            }

            response.Info= "Opération effectuée avec succès";
            response.Status = true;
            return response;
        }
    }
}