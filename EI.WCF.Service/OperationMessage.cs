﻿using System.Runtime.Serialization;

namespace EI.WCF.Service
{
    [DataContract]
    public class OperationMessage
    {
        [DataMember]
        public string Token { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public double Operand1 { get; set; }

        [DataMember]
        public double Operand2 { get; set; }

        [DataMember]
        public double Result { get; set; }

        [DataMember]
        public bool Status { get; set; }

        [DataMember]
        public string Info { get; set; }
    }
}