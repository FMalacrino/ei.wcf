﻿using System;
using System.ServiceModel;
using EI.WCF.Service;

namespace EI.WCF.Client
{
    internal class Program
    {
        static private OperationClient service;

        private static void Main(string[] args)
        {
            //Uri baseAddress = new Uri("http://localhost:8000/EI.WCF.Service/CalculatorService");

            try
            {
                //EndpointAddress endpointAddress = new EndpointAddress(baseAddress);
                //service = ChannelFactory<IOperation>.CreateChannel(new BasicHttpBinding(), endpointAddress);

                service = new OperationClient();

                Console.WriteLine(Calculate(5, 3, "Add"));
                Console.WriteLine(Calculate(5, 3, "Subtract"));
                Console.WriteLine(Calculate(5, 3, "SquareRoot"));

                Console.WriteLine("Press <Enter> to terminate the service.");
                Console.WriteLine();
                Console.ReadLine();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("An exception occurred: {0}", ce.Message);
            }
        }

        static private string Calculate(double a, double b, string operation)
        {
            OperationMessage message = new OperationMessage()
            {
                Token = "token",
                Operand1 = a,
                Operand2 = b,
            };

            message.Name = operation;
            var response = service.Calculate(message);
            if (!response.Status)
                return $"{operation}: {response.Info}";
            else
                return $"{a} {operation} {b} = {response.Result}";
        }
    }
}